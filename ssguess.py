import socket

# Configurar el socket
HOST = 'localhost'
PORT = 12348

try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        data = s.recv(1024)
        print("Dato recibido:", data.decode())
except ConnectionResetError as e:
    print("Error de conexión:", e)
