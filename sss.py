import socket
import numpy as np

def generate_polynomial(secret, k, p):
    # Generar un polinomio aleatorio de grado k-1 en el campo de los números primos
    coefficients = np.random.randint(1, p, size=k-1)
    coefficients = np.insert(coefficients, 0, secret)
    return coefficients

def split_secret(polynomial, n, p):
    # Dividir el polinomio en n partes en el campo de los números primos
    x = np.arange(1, n+1)
    shares = np.polyval(polynomial, x) % p
    return shares

def reconstruct_secret(shares, k, p):
    # Reconstruir el secreto utilizando al menos k partes en el campo de los números primos
    x = np.arange(1, k+1)
    polynomial = np.polyfit(x, shares[:k], k-1)
    return int(polynomial[0]) % p

def ingresar(shares, k):
    input_shares = []
    count = 0
    for i in range(k):
        share = None
        while share is None or share not in shares:
            share = int(input("Ingrese la parte {}: ".format(i+1)))
            if share not in shares:
                print("La parte ingresada no corresponde a una de las partes del secreto.")
                if count == 3:
                    print("Demasiados intentos fallidos.")
                    return None
        input_shares.append(share)
    return input_shares


# Ejemplo de uso
secret = 101
n = 5
k = (n//2) + 1
p = 1021 # Número primo para la aritmética modular

polynomial = generate_polynomial(secret, k, p)
shares = split_secret(polynomial, n, p)
original_shares = shares.copy()

print("Partes compartidas:", shares)

# Configurar el socket
HOST = 'localhost'
PORT = 12348

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    while True:
        conn, addr = s.accept()
        with conn:
            print('Conectado por', addr)
            if len(shares) > 0:
                share = shares[0]
                shares = shares[1:]
                conn.sendall(str(share).encode())
            else:
                print("No quedan más shares para enviar.")
                break  # Salir del bucle si no quedan más shares

# Sección de reconstrucción del secreto
if len(shares) == 0:
    print("Comenzando reconstrucción del secreto...")
    # Aquí debes colocar la lógica para reconstruir el secreto
    input_compartidos = ingresar(original_shares, k) # Ingresar al menos k partes para reconstruir el secreto

    if input_compartidos is not None:
        reconstructed_secret = reconstruct_secret(input_compartidos, k, p)
        print("Secreto reconstruido:", reconstructed_secret)



